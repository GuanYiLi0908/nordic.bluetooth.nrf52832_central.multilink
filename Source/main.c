/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief BLE LED Button Service central and client application main file.
 *
 * This example can be a central for up to 8 peripherals.
 * The peripheral is called ble_app_blinky and can be found in the ble_peripheral
 * folder.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "app_uart.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "ble_nus_c.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_ble_scan.h"

#include "peer_manager.h"

#include "Communication.h"
#include "BluetoothNetwork.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


//#define APP_BLE_CONN_CFG_TAG      1                                     /**< Tag that refers to the BLE stack configuration that is set with @ref sd_ble_cfg_set. The default tag is @ref APP_BLE_CONN_CFG_TAG. */
//#define APP_BLE_OBSERVER_PRIO     3                                     /**< BLE observer priority of the application. There is no need to modify this value. */

//#define UART_TX_BUF_SIZE        256                                     /**< UART TX buffer size. */
//#define UART_RX_BUF_SIZE        256                                     /**< UART RX buffer size. */

//#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

//#define ECHOBACK_BLE_UART_DATA  1                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */

//#define CENTRAL_SCANNING_LED      BSP_BOARD_LED_0
//#define CENTRAL_CONNECTED_LED     BSP_BOARD_LED_1
//#define LEDBUTTON_LED             BSP_BOARD_LED_2                       /**< LED to indicate a change of state of the Button characteristic on the peer. */

#define LEDBUTTON_BUTTON          BSP_BUTTON_0                          /**< Button that writes to the LED characteristic of the peer. */
#define BUTTON_DETECTION_DELAY    APP_TIMER_TICKS(50)                   /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. */
//BLE_NUS_C_ARRAY_DEF(m_ble_nus_c, NRF_SDH_BLE_CENTRAL_LINK_COUNT);           /**< LED button client instances. */
//BLE_DB_DISCOVERY_ARRAY_DEF(m_db_disc, NRF_SDH_BLE_CENTRAL_LINK_COUNT);  /**< Database discovery module instances. */
//NRF_BLE_SCAN_DEF(m_scan);                                               /**< Scanning Module instance. */

//static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

///**@brief NUS UUID. */
//static ble_uuid_t const m_nus_uuid =
//{
//    .uuid = BLE_UUID_NUS_SERVICE,
//    .type = NUS_SERVICE_UUID_TYPE
//};


/**@brief Function for handling asserts in the SoftDevice.
 *
 * @details This function is called in case of an assert in the SoftDevice.
 *
 * @warning This handler is only an example and is not meant for the final product. You need to analyze
 *          how your product is supposed to react in case of an assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing assert call.
 * @param[in] p_file_name  File name of the failing assert call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
	  switch (event)
    {
        case BSP_EVENT_KEY_0:
						bsp_board_led_invert(2);
						TestingFnc();
            break;

        case BSP_EVENT_KEY_1:
						bsp_board_led_invert(3);
            break;

        default:
            return; // no implementation needed
    }
}

/**@brief Function for initializing buttons and leds. */
static void BSPperipheral_Initialization(void)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);
}

///**@brief Function for starting scanning. */
//static void BluetoothScan_Start(void)
//{
//    ret_code_t ret;

//    ret = nrf_ble_scan_start(&m_scan);
//    APP_ERROR_CHECK(ret);
//	
//    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
//    APP_ERROR_CHECK(ret);
//}

//static void scan_evt_handler(scan_evt_t const * p_scan_evt)
//{
//    ret_code_t err_code;

//    switch(p_scan_evt->scan_evt_id)
//    {
//        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
//        {
//            err_code = p_scan_evt->params.connecting_err.err_code;
//            APP_ERROR_CHECK(err_code);
//        } break;

//				case NRF_BLE_SCAN_EVT_CONNECTED:
//        {
//            ble_gap_evt_connected_t const * p_connected =
//                               p_scan_evt->params.connected.p_connected;
//            // Scan is automatically stopped by the connection.
//            NRF_LOG_INFO("Connecting to target %02x%02x%02x%02x%02x%02x",
//                      p_connected->peer_addr.addr[0],
//                      p_connected->peer_addr.addr[1],
//                      p_connected->peer_addr.addr[2],
//                      p_connected->peer_addr.addr[3],
//                      p_connected->peer_addr.addr[4],
//                      p_connected->peer_addr.addr[5]
//                      );
//        } break;
//				
//				case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
//        {
//            NRF_LOG_INFO("Scan timed out.");
//            BluetoothScan_Start();
//        } break;
//				
//        default:
//            break;
//    }
//}


///**@brief Function for initializing the scanning and setting the filters.
// */
//static void BluetoothScan_Initialization(void)
//{
//    ret_code_t          err_code;
//    nrf_ble_scan_init_t init_scan;

//    memset(&init_scan, 0, sizeof(init_scan));

//    init_scan.connect_if_match = true;
//    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

//    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
//    APP_ERROR_CHECK(err_code);

//    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_UUID_FILTER, &m_nus_uuid);
//    APP_ERROR_CHECK(err_code);

//    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_UUID_FILTER, false);
//    APP_ERROR_CHECK(err_code);
//}

///**@brief Function for handling characters received by the Nordic UART Service (NUS).
// *
// * @details This function takes a list of characters of length data_len and prints the characters out on UART.
// *          If @ref ECHOBACK_BLE_UART_DATA is set, the data is sent back to sender.
// */
//static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
//{
//    ret_code_t ret_val;

//    NRF_LOG_DEBUG("Receiving data.");
//    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);

//    for (uint32_t i = 0; i < data_len; i++)
//    {
//        do
//        {
//            ret_val = app_uart_put(p_data[i]);
//            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
//            {
//                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
//                APP_ERROR_CHECK(ret_val);
//            }
//        } while (ret_val == NRF_ERROR_BUSY);
//    }
//    if (p_data[data_len-1] == '\r')
//    {
//        while (app_uart_put('\n') == NRF_ERROR_BUSY);
//    }
//    if (ECHOBACK_BLE_UART_DATA)
//    {
//        // Send data back to the peripheral.
//        do
//        {
//            ret_val = ble_nus_c_string_send(&m_ble_nus_c[0], p_data, data_len);
//            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
//            {
//                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
//                APP_ERROR_CHECK(ret_val);
//            }
//        } while (ret_val == NRF_ERROR_BUSY);
//    }
//}

///**@brief Handles events coming from the LED Button central module.
// *
// * @param[in] p_lbs_c     The instance of LBS_C that triggered the event.
// * @param[in] p_lbs_c_evt The LBS_C event.
// */
//static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
//{
//    switch (p_ble_nus_evt->evt_type)
//    {
//        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
//        {
//            ret_code_t err_code;

//					  NRF_LOG_INFO("Discovery complete.");
//            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
//            APP_ERROR_CHECK(err_code);

//            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
//            APP_ERROR_CHECK(err_code);
//            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
//        } break; 

//        case BLE_NUS_C_EVT_NUS_TX_EVT:
//        {
//					ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
//        } break; 

//				case BLE_NUS_C_EVT_DISCONNECTED:
//            NRF_LOG_INFO("Disconnected.");
//            BLE_Scan_Start();
//            break;
//				
//        default:
//            // No implementation needed.
//            break;
//    }
//}


///**@brief Function for handling BLE events.
// *
// * @param[in]   p_ble_evt   Bluetooth stack event.
// * @param[in]   p_context   Unused.
// */
//static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
//{
//    ret_code_t err_code;

//    // For readability.
//    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

//    switch (p_ble_evt->header.evt_id)
//    {
//        // Upon connection, check which peripheral is connected, initiate DB
//        // discovery, update LEDs status, and resume scanning, if necessary.
//        case BLE_GAP_EVT_CONNECTED:
//        {
//            NRF_LOG_INFO("Connection 0x%x established, starting DB discovery.",
//                         p_gap_evt->conn_handle);

//            APP_ERROR_CHECK_BOOL(p_gap_evt->conn_handle < NRF_SDH_BLE_CENTRAL_LINK_COUNT);

//            err_code = ble_nus_c_handles_assign(&m_ble_nus_c[p_gap_evt->conn_handle],
//                                                p_gap_evt->conn_handle,
//                                                NULL);
//            APP_ERROR_CHECK(err_code);

//            err_code = ble_db_discovery_start(&m_db_disc[p_gap_evt->conn_handle],
//                                              p_gap_evt->conn_handle);
//            if (err_code != NRF_ERROR_BUSY)
//            {
//                APP_ERROR_CHECK(err_code);
//            }

//            // Update LEDs status and check whether it is needed to look for more
//            // peripherals to connect to.
//            bsp_board_led_on(CENTRAL_CONNECTED_LED);
//            if (ble_conn_state_central_conn_count() == NRF_SDH_BLE_CENTRAL_LINK_COUNT)
//            {
//                bsp_board_led_off(CENTRAL_SCANNING_LED);
//            }
//            else
//            {
//                // Resume scanning.
//                bsp_board_led_on(CENTRAL_SCANNING_LED);
//                BluetoothScan_Start();
//            }
//        } break; // BLE_GAP_EVT_CONNECTED

//        // Upon disconnection, reset the connection handle of the peer that disconnected, update
//        // the LEDs status and start scanning again.
//        case BLE_GAP_EVT_DISCONNECTED:
//        {
//            NRF_LOG_INFO("LBS central link 0x%x disconnected (reason: 0x%x)",
//                         p_gap_evt->conn_handle,
//                         p_gap_evt->params.disconnected.reason);

//            if (ble_conn_state_central_conn_count() == 0)
//            {
//                err_code = app_button_disable();
//                APP_ERROR_CHECK(err_code);

//                // Turn off the LED that indicates the connection.
//                bsp_board_led_off(CENTRAL_CONNECTED_LED);
//            }

//            // Start scanning.
//            BluetoothScan_Start();

//            // Turn on the LED for indicating scanning.
//            bsp_board_led_on(CENTRAL_SCANNING_LED);

//        } break;

//        case BLE_GAP_EVT_TIMEOUT:
//        {
//            // Timeout for scanning is not specified, so only the connection requests can time out.
//            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
//            {
//                NRF_LOG_DEBUG("Connection request timed out.");
//            }
//        } break;

//				case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
//            // Pairing not supported.
//            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
//            APP_ERROR_CHECK(err_code);
//            break;
//				
//        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
//        {
//            NRF_LOG_DEBUG("BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST.");
//            // Accept parameters requested by peer.
//            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
//                                        &p_gap_evt->params.conn_param_update_request.conn_params);
//            APP_ERROR_CHECK(err_code);
//        } break;

//        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
//        {
//            NRF_LOG_DEBUG("PHY update request.");
//            ble_gap_phys_t const phys =
//            {
//                .rx_phys = BLE_GAP_PHY_AUTO,
//                .tx_phys = BLE_GAP_PHY_AUTO,
//            };
//            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
//            APP_ERROR_CHECK(err_code);
//        } break;

//        case BLE_GATTC_EVT_TIMEOUT:
//        {
//            // Disconnect on GATT client timeout event.
//            NRF_LOG_DEBUG("GATT client timeout.");
//            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
//                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
//            APP_ERROR_CHECK(err_code);
//        } break;

//        case BLE_GATTS_EVT_TIMEOUT:
//        {
//            // Disconnect on GATT server timeout event.
//            NRF_LOG_DEBUG("GATT server timeout.");
//            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
//                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
//            APP_ERROR_CHECK(err_code);
//        } break;

//        default:
//            // No implementation needed.
//            break;
//    }
//}

///**@brief   Function for handling app_uart events.
// *
// * @details This function receives a single character from the app_uart module and appends it to
// *          a string. The string is sent over BLE when the last character received is a
// *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
// */
//void uart_event_handle(app_uart_evt_t * p_event)
//{
//    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
//    static uint16_t index = 0;
//    uint32_t ret_val;

//    switch (p_event->evt_type)
//    {
//        /**@snippet [Handling data from UART] */
//        case APP_UART_DATA_READY:
//            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
//            index++;

//            if ((data_array[index - 1] == '\n') || (index >= (m_ble_nus_max_data_len)))
//            {
//                NRF_LOG_DEBUG("Ready to send data over BLE NUS");
//                NRF_LOG_HEXDUMP_DEBUG(data_array, index);

//                do
//                {
//                    ret_val = ble_nus_c_string_send(&m_ble_nus_c[0], data_array, index);
//                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_RESOURCES) )
//                    {
//                        APP_ERROR_CHECK(ret_val);
//                    }
//                } while (ret_val == NRF_ERROR_RESOURCES);

//                index = 0;
//            }
//            break;

//        /**@snippet [Handling data from UART] */
//        case APP_UART_COMMUNICATION_ERROR:
//            NRF_LOG_ERROR("Communication error occurred while handling UART.");
//            APP_ERROR_HANDLER(p_event->data.error_communication);
//            break;

//        case APP_UART_FIFO_ERROR:
//            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
//            APP_ERROR_HANDLER(p_event->data.error_code);
//            break;

//        default:
//            break;
//    }
//}

///**@brief Function for initializing the UART. */
//static void UART_Initialization(void)
//{
//    ret_code_t err_code;

//    app_uart_comm_params_t const comm_params =
//    {
//        .rx_pin_no    = RX_PIN_NUMBER,
//        .tx_pin_no    = TX_PIN_NUMBER,
//        .rts_pin_no   = RTS_PIN_NUMBER,
//        .cts_pin_no   = CTS_PIN_NUMBER,
//        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
//        .use_parity   = false,
//        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
//    };

//    APP_UART_FIFO_INIT(&comm_params,
//                       UART_RX_BUF_SIZE,
//                       UART_TX_BUF_SIZE,
//                       uart_event_handle,
//                       APP_IRQ_PRIORITY_LOWEST,
//                       err_code);

//    APP_ERROR_CHECK(err_code);
//}

///**@brief LED Button collector initialization. */
//static void NUSc_Initialization(void)
//{
//    ret_code_t       err_code;
//    ble_nus_c_init_t init;

//    init.evt_handler = ble_nus_c_evt_handler;

//    for (uint32_t i = 0; i < NRF_SDH_BLE_CENTRAL_LINK_COUNT; i++)
//    {
//        err_code = ble_nus_c_init(&m_ble_nus_c[i], &init);
//        APP_ERROR_CHECK(err_code);
//    }
//}


///**@brief Function for initializing the BLE stack.
// *
// * @details Initializes the SoftDevice and the BLE event interrupts.
// */
//static void BluetoothProtocolStack_Initialization(void)
//{
//    ret_code_t err_code;

//    err_code = nrf_sdh_enable_request();
//    APP_ERROR_CHECK(err_code);

//    // Configure the BLE stack using the default settings.
//    // Fetch the start address of the application RAM.
//    uint32_t ram_start = 0;
//    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
//    APP_ERROR_CHECK(err_code);

//    // Enable BLE stack.
//    err_code = nrf_sdh_ble_enable(&ram_start);
//    APP_ERROR_CHECK(err_code);

//    // Register a handler for BLE events.
//    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
//}


///**@brief Function for handling database discovery events.
// *
// * @details This function is a callback function to handle events from the database discovery module.
// *          Depending on the UUIDs that are discovered, this function forwards the events
// *          to their respective services.
// *
// * @param[in] p_event  Pointer to the database discovery event.
// */
//static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
//{
//    NRF_LOG_DEBUG("call to ble_lbs_on_db_disc_evt for instance %d and link 0x%x!",
//                  p_evt->conn_handle,
//                  p_evt->conn_handle);

//    ble_nus_c_on_db_disc_evt(&m_ble_nus_c[p_evt->conn_handle], p_evt);
//}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details This function handles any pending log operations, then sleeps until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


int main(void)
{
		ret_code_t err_code;

		err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
	
		/* nRF Log Initialization */
    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEFAULT_BACKENDS_INIT();
	
		UART_Initialization();
	
		/* Board Support Package Initialzation */
		BSPperipheral_Initialization();
    power_management_init();
	
		/* Bluetooth & Protocol Stack Initialization */
    BLE_ProtocolStack_Initialization();

		/* GATT Protocol Initialzation */
    err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
	
		/* Bluetooth Database Discovery Module Initialization */
		BLE_Discovery_Initialization();
	
		/* Nordic UART Service (NUS) client Initialization */
    NUSc_Initialization();

		/* Bluetooth Connect state Initialization */
    ble_conn_state_init();
		
		err_code = pm_init();
		APP_ERROR_CHECK(err_code);
		
		/* Bluetooth Filiter Setting and scanning*/
		BLE_Scan_Initialization();	
		NRF_LOG_INFO("System Initialization Finish");

		BLE_Scan_Start();
    NRF_LOG_INFO("Bluetooth Multilink Central Scanning Start");

    for (;;)
    {
        idle_state_handle();
    }
}
