

#ifndef __BLUETOOTHNETWORK_H
#define __BLUETOOTHNETWORK_H
#ifdef __cplusplus
 extern "C" {
#endif
		
//		#include <stdint.h>
//		#include "nrf_ble_gatt.h"
//		#include "ble_advertising.h"
//		#include "ble_conn_params.h"
		
		/* GAP */
//		#define DEVICE_NAME               "unicoRN_nRF"   											/**< Name of device. Will be included in the advertising data. */
//		#define MIN_CONN_INTERVAL         MSEC_TO_UNITS(20, UNIT_1_25_MS)       /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
//		#define MAX_CONN_INTERVAL         MSEC_TO_UNITS(75, UNIT_1_25_MS)       /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
//		#define SLAVE_LATENCY             0                                     /**< Slave latency. */
//		#define CONN_SUP_TIMEOUT          MSEC_TO_UNITS(4000, UNIT_10_MS)       /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */

		/* Advertising */
//		#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)           /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
//		#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)          /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
//		#define MAX_CONN_PARAMS_UPDATE_COUNT    3   														/**< Number of attempts before giving up the connection parameter negotiation. */
		
		/* GATT */
		#define NUS_SERVICE_UUID_TYPE     BLE_UUID_TYPE_VENDOR_BEGIN            /**< UUID type for the Nordic UART Service (vendor specific). */
//		#define APP_ADV_INTERVAL          64                                    /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
//		#define APP_ADV_DURATION          18000                                 /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

		

//		void GAP_Initialization(void);
//		void Connect_Initialization(void);
//		void Advertising_Initialization(void);
//		void BluetoothAdvertising_Start(void);
//		static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
//		static void on_conn_params_evt(ble_conn_params_evt_t * p_evt);
//		static void conn_params_error_handler(uint32_t nrf_error);
//		
//		void GATT_Initialization(void);
//		static void GATTevent_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt);
		
#ifdef __cplusplus
}
#endif
#endif /*__BLUETOOTHNETWORK_H */
